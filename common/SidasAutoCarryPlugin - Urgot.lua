------------------------########################################-------------------------------
------------------------##								Urgot		  					##-------------------------------
------------------------##					 The Deadly Butcher 			##-------------------------------
------------------------########################################-------------------------------
if myHero.charName ~= "Urgot" then return end

function PluginOnLoad()
	AutoCarry.SkillsCrosshair.range = 1250
	--> Main Load
	mainLoad()
	--> Main Menu
	mainMenu()
	--> Tower Table
	towersUpdate()
end

function PluginOnTick()
	Checks()
--
	if Menu.ToggleMuramana then MuramanaToggle() end
--
	if Target then
		if AutoCarry.MainMenu.AutoCarry then
			if EREADY and Menu.useE and GetDistance(Target) < eRange then 
				if IsSACReborn then
					SkillE:ForceCast(Target)
				else
					Cast(SkillE, Target)
				end
			end
			if WREADY and Menu.useW and GetDistance(Target) < wRange then CastSpell(_W) end
			if QREADY and Menu.useQ then CastQ(Target) end
			if RREADY and Menu.useR then CastR(Target) end
		elseif AutoCarry.MainMenu.MixedMode then
			if EREADY and Menu.useE2 and GetDistance(Target) < eRange then
				if IsSACReborn then
					SkillE:ForceCast(Target)
				else
					Cast(SkillE, Target)
				end
			end
			if WREADY and Menu.useW2 and GetDistance(Target) < wRange then CastSpell(_W) end
			if QREADY and Menu.useQ2 then CastQ(Target) end
			if RREADY and Menu.useR2 then CastR(Target) end
		end
	end
--
	if Target and QREADY and Menu.autoQ then 
		if GetDistance(Target) < qRange2 and GetTickCount() - poisonedtimets < 5000 then
			CastSpell(_Q, Target.x, Target.z)
		end
	end
--
	if Menu.rTower then towerTeleport() end
--
	if QREADY and Menu.qFarm and AutoCarry.MainMenu.LaneClear then
		if Minion and not Minion.type == "obj_Turret" and not Minion.dead and GetDistance(Minion) <= qRange and Minion.health < getDmg("Q", Minion, myHero) then 
			CastSpell(_Q, Minion.x, Minion.z)
		else 
			for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
				if minion and not minion.dead and GetDistance(minion) <= qRange and minion.health < getDmg("Q", minion, myHero) then 
					CastSpell(_Q, minion.x, minion.z)
				end
			end
		end
	end
end

function PluginOnDraw()
	--> Ranges
	if not Menu.drawMaster and not myHero.dead then
		if QREADY and Menu.drawQ then
			DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
		end
		if EREADY and Menu.drawE then
			DrawCircle(myHero.x, myHero.y, myHero.z, eRange, 0x00FF00)
		end
	end
end

--> Acid Debuff
function PluginOnCreateObj(obj)
	if obj ~= nil and string.find(obj.name, "UrgotCorrosiveDebuff_buf") then
		for i=1, heroManager.iCount do
			local enemy = heroManager:GetHero(i)
			if enemy.team ~= myHero.team and GetDistance(obj,enemy) < 80 then
				poisonedtime[i] = GetTickCount()
			end
		end
	end
end

function MuramanaToggle()
	if Target and Target.type == myHero.type and GetDistance(Target) <= qRange2 and not MuramanaIsActive() and (AutoCarry.MainMenu.AutoCarry or AutoCarry.MainMenu.MixedMode) then
		MuramanaOn()
	elseif not Target and MuramanaIsActive() then
		MuramanaOff()
	end
end

--> Acid Hunter Cast
function CastQ(target)
	if GetDistance(target) <= qRange2 and GetTickCount()-poisonedtimets < 5000 then
		CastSpell(_Q, target.x, target.z) 
	elseif not Menu.poisonOnly and GetDistance(target) < qRange then
		if IsSACReborn then
			SkillQ:ForceCast(target)
		else
			if not Col(SkillQ, myHero, target) then Cast(SkillQ, target) end
		end
	end
end

function CastR(target)
	if GetDistance(target) <= rRange then
		if CountEnemies(target, 600) <= 3 then CastSpell(_R, target) end
	end
end

function towerTeleport()
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if enemy and GetDistance(enemy) <= rRange and inTurretRange(myHero) then
			if CountEnemies(enemy, 600) <= 3 then CastSpell(_R, enemy) end
		end
	end
end

--> Checks
function Checks()
	Target = AutoCarry.GetAttackTarget()
	Minion = AutoCarry.GetMinionTarget()
--
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
	MREADY = (MuraSlot and myHero:CanUseSpell(MuraSlot) == READY)
--
	if Target then
		for i=1, heroManager.iCount do
			local enemy = heroManager:GetHero(i)
			if enemy.team ~= myHero.team and enemy.charName == Target.charName then
				poisonedtimets = poisonedtime[i]
			end
		end
	end
--
	rRange = 400 + (player:GetSpellData(_R).level*150)
end

--> Main Load
function mainLoad()
--
	if AutoCarry.Skills and VIP_USER then IsSACReborn = true else IsSACReborn = false end
--
	poisonedtimets = 0
	poisonedtime = {}
	poisontime = 0
	towers = {}
--
	qRange, qRange2, wRange, eRange = 1000, 1200, 700, 900
	QREADY, WREADY, EREADY, RREADY = false, false, false, false
	MuraSlot = GetInventorySlotItem(3042)
--
	if IsSACReborn then
		SkillQ = AutoCarry.Skills:NewSkill(false, _Q, qRange, "Urgot Q", AutoCarry.SPELL_LINEAR_COL, 0, false, false, 1.6, 150, 80, true)
		SkillE = AutoCarry.Skills:NewSkill(false, _E, eRange, "Urgot E", AutoCarry.SPELL_CIRCLE, 0, false, false, 1.75, 250, 150, false)
	else
		SkillQ = {spellKey = _Q, range = qRange, speed = 1.6, delay = 150, width = 80, minions = true}
		SkillE = {spellKey = _E, range = eRange, speed = 1.75, delay = 250, width = 150}
	end
--
	Cast = AutoCarry.CastSkillshot
	Menu = AutoCarry.PluginMenu
	Col = AutoCarry.GetCollision
--
	for i=1, heroManager.iCount do
		poisonedtime[i] = 0
	end
end

--> Main Menu
function mainMenu()
	Menu:addParam("sep", "-- Cast Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("autoQ", "Acid Hunter - Auto Fire", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("qFarm", "Acid Hunter - Lane Clear", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("poisonOnly", "Acid Hunter - Poisoned Targets Only", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("ToggleMuramana", "Muramana - Toggle", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("rTower", "Position Reverser - Turret Transfer", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep1", "-- Auto Carry Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ", "Fire Acid Hunter", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useW", "Use Terror Capacitor", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE", "Fire Corrosive Charge", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR", "Use Position Reverser", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep2", "-- Mixed Mode Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ2", "Fire Acid Hunter", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useW2", "Use Terror Capacitor", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE2", "Fire Corrosive Charge", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR2", "Use Position Reverser", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep3", "-- Draw Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("drawMaster", "Disable Draw", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawQ", "Draw - Acid Hunter", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawE", "Draw - Corrosive Charge", SCRIPT_PARAM_ONOFF, false)
end

function CountEnemies(point, range)
	local ChampCount = 0
	for j = 1, heroManager.iCount, 1 do
		local enemyhero = heroManager:getHero(j)
		if myHero.team ~= enemyhero.team and not enemyhero.dead then
			if GetDistance(enemyhero, point) <= range then
				ChampCount = ChampCount + 1
			end
		end
	end
	return ChampCount
end

--> Tower Checks
function towersUpdate()
	for i = 1, objManager.iCount, 1 do
		local obj = objManager:getObject(i)
		if obj and obj.type == "obj_AI_Turret" and obj.health > 0 then
			if not string.find(obj.name, "TurretShrine") and obj.team == player.team then
				table.insert(towers, obj)
			end
		end
	end
end

function inTurretRange(unit)
	local check = false
	for i, tower in ipairs(towers) do
		if tower and tower.health > 0 then
			if math.sqrt((tower.x - unit.x) ^ 2 + (tower.z - unit.z) ^ 2) < 750 then
				check = true
			end
		else
			table.remove(towers, i)
		end
	end
	return check
end