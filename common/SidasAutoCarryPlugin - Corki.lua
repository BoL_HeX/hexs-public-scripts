---------------------#####################################################---------------------
---------------------##										 Corki												##---------------------
---------------------##								Death from Above									##---------------------
---------------------#####################################################---------------------
if myHero.charName ~= "Corki" then return end

function PluginOnLoad()
	AutoCarry.SkillsCrosshair.range = 1300
	--> Load
	mainLoad()
	--> Main Menu
	mainMenu()
end

function PluginOnTick()
	Checks()
	--> Barrage
	if Target and not Target.dead then
		if AutoCarry.MainMenu.AutoCarry then
			if QREADY and Menu.useQ then castQ(Target) end
			if EREADY and Menu.useE and GetDistance(Target) < eRange then 
				CastSpell(_E, Target.x, Target.z) 
			end
			if RREADY and Menu.useR then castR(Target) end
		elseif AutoCarry.MainMenu.MixedMode then
			if QREADY and Menu.useQ2 then castQ(Target) end
			if EREADY and Menu.useE2 and GetDistance(Target) < eRange then
				CastSpell(_E, Target.x, Target.z) 
			end
			if RREADY and Menu.useR2 then castR(Target) end
		end
	end
	--> Missile KS
	if Menu.missileKS and RREADY then missileKS() end
end

function PluginOnDraw()
	--> Ranges
	if not Menu.drawMaster and not myHero.dead then
		if QREADY and Menu.drawQ then
			DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
		end
		if RREADY and Menu.drawR then
			DrawCircle(myHero.x, myHero.y, myHero.z, rRange, 0x00FF00)
		end
	end
end

function PluginOnProcessSpell(unit, spell)
	if unit.isMe and spell.name == "MissileBarrage" then
		missileCount = missileCount + 1
	end
end

--> Phosphorus Bomb Cast
function castQ(target)
	if target and GetDistance(target) <= qRange then
		if GetDistance(target) > 600 then
			Distance = GetDistance(target) - 600
			TargetPos = Vector(target.x, target.y, target.z)
			MyPos = Vector(myHero.x, myHero.y, myHero.z)
			qPred2 = TargetPos + (TargetPos-MyPos)*((-Distance/GetDistance(target)))
			CastSpell(_Q, qPred2.x, qPred2.z)
		elseif GetDistance(target) <= 600 then
			CastSpell(_Q, target.x, target.z)
		end
	end
end

--> Missile KS
function missileKS()
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if missileCount == 2 then rDmg = getDmg("R", enemy, myHero)*1.5 
			else rDmg = getDmg("R", enemy, myHero) 
		end
		if enemy and not enemy.dead and enemy.health < rDmg then castR(enemy) end
	end
end

function castR(target)
	if IsSACReborn then
		SkillR:ForceCast(target)
	else
		Cast(SkillR, target)
	end
end

--> Checks
function Checks()
	Target = AutoCarry.GetAttackTarget()
	if missileCount > 2 then missileCount = 0 end
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
end

--> Main Load
function mainLoad()
	if AutoCarry.Skills and VIP_USER then IsSACReborn = true else IsSACReborn = false end
	qRange, eRange, rRange = 775, 600, 1300
	QREADY, WREADY, RREADY = false, false, false
	missileCount = 0
	if IsSACReborn then
		SkillR = AutoCarry.Skills:NewSkill(true, _R, rRange, "Missile Barrage", AutoCarry.SPELL_LINEAR_COL, 0, false, false, 2.0, 175, 60, true)
	else
		SkillR = {spellKey = _R, range = rRange, speed = 2.0, delay = 175, width = 60, minions = true}
	end

	Menu = AutoCarry.PluginMenu
	Cast = AutoCarry.CastSkillshot
end

--> Main Menu
function mainMenu()
	Menu:addParam("sep", "-- Auto Carry Abilities --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ", "Load Phosphorus Bombs", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE", "Fire Gatling Gun", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR", "Prep Missile Barrage", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep1", "-- Mixed Mode Abilities  --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ2", "Load Phosphorus Bombs", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE2", "Fire Gatling Gun", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR2", "Prep Missile Barrage", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep2", "-- Cast Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("missileKS", "Missile - Kill Steal", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep3", "-- Draw Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("drawMaster", "Disable Draw", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawQ", "Draw - Phosphorus Bombs", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawR", "Draw - Missile Barrage", SCRIPT_PARAM_ONOFF, false)
end