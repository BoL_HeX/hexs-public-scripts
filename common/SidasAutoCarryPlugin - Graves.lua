------------------------########################################-------------------------------
------------------------##								Graves							##-------------------------------
------------------------##							Running Riot					##-------------------------------
------------------------########################################-------------------------------
if myHero.charName ~= "Graves" then return end

if VIP_USER then
	require "Collision"
	col = Collision(1000, 2000, 0.25, 120)
end

function PluginOnLoad()
	AutoCarry.SkillsCrosshair.range = 1050
	--> Main Load
	mainLoad()
	--> Main Menu
	mainMenu()
end

function PluginOnTick()
	Checks()
	if Target then
		if AutoCarry.MainMenu.AutoCarry then
			if QREADY and Menu.useQ and GetDistance(Target) < qRange then Cast(SkillQ, Target) end
			if WREADY and Menu.useW and GetDistance(Target) < wRange then Cast(SkillW, Target) end
			if EREADY and Menu.useE then castE() end
			if RREADY and Menu.useR then castR(Target) end
		elseif AutoCarry.MainMenu.MixedMode then
			if QREADY and Menu.useQ2 and GetDistance(Target) < qRange then Cast(SkillQ, Target) end
			if WREADY and Menu.useW2 and GetDistance(Target) < wRange then Cast(SkillW, Target) end
			if EREADY and Menu.useE2 then castE() end
			if RREADY and Menu.useR2 then castR(Target) end
		end
	end
	if Menu.ultKS or Menu.buckshotKS then KillSteal() end
end

function PluginOnDraw()
	--> Ranges
	if not Menu.drawMaster and not myHero.dead then
		if QREADY and Menu.drawQ then
			DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
		end
		if RREADY and Menu.drawR then
			DrawCircle(myHero.x, myHero.y, myHero.z, rRange, 0x00FF00)
		end
	end
end

--> Quickdraw Cast
function castE()
	CastSpell(_E, mousePos.x, mousePos.z)	
end

function castR(target)
	rPred = AutoCarry.GetPrediction(SkillR, target)
	if VIP_USER and rPred then
		local Enemies = 0
		local maxDistance = myHero + (Vector(rPred) - myHero):normalized()*1000
		local collision, champs = col:GetHeroCollision(myHero, maxDistance, HERO_ENEMY)
		if collision and champs then
			for i, champs in pairs(champs) do
				Enemies = Enemies + 1
			end
			if Enemies > Menu.rEnemies then Cast(SkillR, target) end
		end
	else 
		Cast(SkillR, target)
	end
end

function KillSteal()
	for i, enemy in ipairs(GetEnemyHeroes()) do
		local qDmg = getDmg("Q", enemy, myHero)
		local rDmg = getDmg("R", enemy, myHero)
		if enemy and not enemy.dead then
			if QREADY and Menu.buckshotKS and enemy.health < qDmg then
				Cast(SkillQ, enemy)
			elseif RREADY and Menu.ultKS and enemy.health < rDmg then
				Cast(SkillR, enemy)
			elseif QREADY and RREADY and Menu.ultKS and enemy.health < (qDmg + rDmg) then
				Cast(SkillQ, enemy)
				Cast(SkillR, enemy)
			end
		end
	end
end

--> Checks
function Checks()
	Target = AutoCarry.GetAttackTarget()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
end

--> Main Load
function mainLoad()
	qRange, wRange, eRange, rRange = 750, 950, 580, 1000
	QREADY, WREADY, EREADY, RREADY = false, false, false, false
	SkillQ = {spellKey = _Q, range = qRange, speed = 2.0, delay = 250}
	SkillW = {spellKey = _W, range = wRange, speed = 1.65, delay = 300}
	SkillR = {spellKey = _R, range = rRange, speed = 2.0, delay = 250}
	Cast = AutoCarry.CastSkillshot
	Menu = AutoCarry.PluginMenu
end

--> Main Menu
function mainMenu()
	Menu:addParam("sep", "-- Cast Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("ultKS", "Kill with Collateral Damage", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("buckshotKS", "Kill with Buckshot", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("rEnemies", "Collateral Damage - Min Enemies",SCRIPT_PARAM_SLICE, 2, 1, 5, 0)
	Menu:addParam("sep1", "-- Auto Carry Abilities --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ", "Load Buckshot", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useW", "Deploy Smoke Screen", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE", "Quickdraw Gun", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR", "Prep Collateral Damage", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep2", "-- Mixed Mode Abilities --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ2", "Load Buckshot", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useW2", "Deploy Smoke Screen", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE2", "Quickdraw Gun", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR2", "Prep Collateral Damage", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("sep3", "-- Draw Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("drawMaster", "Disable Draw", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawQ", "Draw - Buckshot", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawR", "Draw - Collateral Damage", SCRIPT_PARAM_ONOFF, false)
end