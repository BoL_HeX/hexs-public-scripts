if myHero.charName ~= "MonkeyKing" then return end

function PluginOnLoad()
	AutoCarry.SkillsCrosshair.range = 700
	--> Load
	mainLoad()
	--> Main Menu
	mainMenu()
end

function PluginOnTick()
	Checks()
	if Menu2.AutoCarry and Target then
		if UltimateActive then myHero:MoveTo(mousePos.x, mousePos.z) end
		if QREADY and Menu.useQ and GetDistance(Target) <= qRange and (GetDistance(Target) >= 200 or Target.health <= getDmg("Q", Target, myHero)) then CastSpell(_Q) end
		if EREADY and Menu.useE and GetDistance(Target) <= eRange and (GetDistance(Target) >= 325 or Target.health <= getDmg("E", Target, myHero)) then CastSpell(_E, Target) end
		if RREADY and Menu.useR and CountEnemies(myHero, rRange) >= Menu.rEnemies then CastSpell(_R) end
	end
	if Menu2.MixedMode and Target then
		if QREADY and Menu.useQ and GetDistance(Target) <= qRange and (GetDistance(Target) >= 200 or Target.health <= getDmg("Q", Target, myHero)) then CastSpell(_Q) end
	end
end

function OnAttacked()
	if Menu2.AutoCarry and Target then
		if QREADY and Menu.useQ and GetDistance(Target) <= qRange then CastSpell(_Q) end
	end
end

function PluginOnCreateObj(obj)
	if obj.name == "monkey_king_ult_spin.troy" and GetDistance(obj) < 100 then UltimateActive = true end
end

function PluginOnDeleteObj(obj)
	if obj.name == "monkey_king_ult_spin.troy" and GetDistance(obj) < 100 then UltimateActive = false end
end

--> Checks
function Checks()
	Target = AutoCarry.GetAttackTarget()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
end

--> Main Load
function mainLoad()
	qRange, eRange, rRange = 300, 650, 200
	Menu = AutoCarry.PluginMenu
	Menu2 = AutoCarry.MainMenu
end

function CountEnemies(point, range)
	local ChampCount = 0
	for j = 1, heroManager.iCount, 1 do
		local enemyhero = heroManager:getHero(j)
		if myHero.team ~= enemyhero.team and ValidTarget(enemyhero, rRange+150) then
			if GetDistance(enemyhero, point) <= range then
				ChampCount = ChampCount + 1
			end
		end
	end		
	return ChampCount
end

--> Main Menu
function mainMenu()
	Menu:addParam("sep", "-- Cast Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("rEnemies", "Cyclone - Min Enemies",SCRIPT_PARAM_SLICE, 2, 1, 5, 0)
	Menu:addParam("sep1", "-- Ability Options --", SCRIPT_PARAM_INFO, "")
	Menu:addParam("useQ", "Use Crushing Blow", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE", "Use Nimbus Strike", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR", "Use Cyclone", SCRIPT_PARAM_ONOFF, true)
end