--[[	Trundle Helper 1.1.1 by HeX	]]--
if myHero.charName ~= "Trundle" then return end

--[[	Ranges	]]--
local qRange = 300
local wRange = 900
local eRange = 1000
local rRange = 700
local ts
--[[	Attack	]]--
local lastBasicAttack = 0
local swingDelay = 0.25
local startAttackSpeed = 0.625
local swing = 0
--[[	Ready	]]--
local ignite = nil
local BRKSlot, DFGSlot, HXGSlot, BWCSlot, TMTSlot, RAHSlot, RNDSlot = nil, nil, nil, nil, nil, nil, nil
local QREADY, WREADY, EREADY, RREADY, BRKREADY, DFGREADY, HXGREADY, BWCREADY, TMTREADY, RAHREADY, RNDREADY = false, false, false, false, false, false, false, false, false, false, false

function OnLoad()
	PrintChat("<font color='#CCCCCC'> >> Trundle Helper 1.1.1 loaded! <<</font>")
	PrintChat("<font color='#CCCCCC'> >> Time to Troll <<</font>")
	THConfig = scriptConfig("Trundle Helper", "TrundleHelper")
	THConfig:addParam("scriptActive", "Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	THConfig:addParam("pillarBlock", "Block with Pillar", SCRIPT_PARAM_ONKEYDOWN, false, 71)
	THConfig:addParam("useUlt", "Use Ultimate in Combo", SCRIPT_PARAM_ONKEYTOGGLE, true, 90)
	THConfig:addParam("dynaPillar", "Dynamic Pillar Control", SCRIPT_PARAM_ONKEYTOGGLE, true, 88)
	THConfig:addParam("autoignite", "Auto Ignite", SCRIPT_PARAM_ONOFF, true)
	THConfig:addParam("drawcirclesSelf", "Draw Circles - Self", SCRIPT_PARAM_ONOFF, true)
	THConfig:addParam("drawcirclesEnemy", "Draw Circles - Enemy", SCRIPT_PARAM_ONOFF, true)
	THConfig:permaShow("scriptActive")
	THConfig:permaShow("pillarBlock")

	THConfig:permaShow("dynaPillar")
	THConfig:permaShow("useUlt")
	
	ts = TargetSelector(TARGET_LOW_HP, eRange+100, DAMAGE_PHYSICAL)
	ts.name = "Trundle"
	THConfig:addTS(ts)
	
	
	if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
	elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2 end 
end

function OnProcessSpell(unit, spell)
	if unit.isMe and (spell.name:find("Attack") ~= nil) then
		swing = 1
		lastBasicAttack = os.clock()
	end
end

function OnTick()
	ts:update()
	
	AttackDelay = 1/(myHero.attackSpeed*startAttackSpeed)
	if swing == 1 and os.clock() > lastBasicAttack + AttackDelay then
		swing = 0
	end

	BRKSlot, DFGSlot, HXGSlot, BWCSlot, TMTSlot, RAHSlot, RNDSlot = GetInventorySlotItem(3153), GetInventorySlotItem(3128), GetInventorySlotItem(3146), GetInventorySlotItem(3144), GetInventorySlotItem(3077), GetInventorySlotItem(3074),  GetInventorySlotItem(3143)
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
	DFGREADY = (DFGSlot ~= nil and myHero:CanUseSpell(DFGSlot) == READY)
	HXGREADY = (HXGSlot ~= nil and myHero:CanUseSpell(HXGSlot) == READY)
	BWCREADY = (BWCSlot ~= nil and myHero:CanUseSpell(BWCSlot) == READY)
	BRKREADY = (BRKSlot ~= nil and myHero:CanUseSpell(BRKSlot) == READY)
	TMTREADY = (TMTSlot ~= nil and myHero:CanUseSpell(TMTSlot) == READY)
	RAHREADY = (RAHSlot ~= nil and myHero:CanUseSpell(RAHSlot) == READY)
	RNDREADY = (RNDSlot ~= nil and myHero:CanUseSpell(RNDSlot) == READY)
	IREADY = (ignite ~= nil and myHero:CanUseSpell(ignite) == READY)
	
--[[	Ignite	]]--
	if THConfig.autoignite then    
		if IREADY then
			local ignitedmg = 0    
			for i = 1, heroManager.iCount, 1 do
				local enemyhero = heroManager:getHero(i)
				if ValidTarget(enemyhero,600) then
					ignitedmg = 50 + 20 * myHero.level
					if enemyhero.health <= ignitedmg then
						CastSpell(ignite, enemyhero)
					end
				end
			end
		end
	end

	--[[	Pillar Block	]]--
	if ts.target ~= nil and EREADY then
		BlockwithPillar()
	end
	--[[if ts.target ~= nil and MyPredictedDirection() == "Flee" then
		PrintChat("Fleeing")
		else PrintChat("Chasing")
	end]]
	
	--[[	Combo	]]--
	if THConfig.scriptActive and ts.target ~= nil then
		if DFGREADY then CastSpell(DFGSlot, ts.target) end
		if HXGREADY then CastSpell(HXGSlot, ts.target) end
		if BWCREADY then CastSpell(BWCSlot, ts.target) end
		if BRKREADY then CastSpell(BRKSlot, ts.target) end
		if TMTREADY and GetDistance(ts.target) < 275 then CastSpell(TMTSlot) end
		if RAHREADY and GetDistance(ts.target) < 275 then CastSpell(RAHSlot) end
		if RNDREADY and GetDistance(ts.target) < 275 then CastSpell(RNDSlot) end
		local QDMG = getDmg("Q", ts.target, myHero)
		if swing == 0  then
      if GetDistance(ts.target) < qRange then
        myHero:Attack(ts.target)
				elseif QREADY and ts.target.health < QDMG and GetDistance(ts.target) < qRange then
					CastSpell(_Q)
					myHero:Attack(ts.target)
					swing = 0
      end
			if WREADY and GetDistance(ts.target) <= 400 then  
				CastSpell(_W, myHero.x, myHero.z)
				myHero:Attack(ts.target)
				elseif GetDistance(ts.target) >= 400 and GetDistance(ts.target) <= wRange then
					CastSpell(_W, ts.target.x, ts.target.z)
					myHero:Attack(ts.target)
      end
			elseif swing == 1 then
			if QREADY and os.clock() - lastBasicAttack > swingDelay and GetDistance(ts.target) < qRange then
				CastSpell(_Q)
				myHero:Attack(ts.target)
				swing = 0
			end
			if RREADY and THConfig.useUlt and GetDistance(ts.target) < rRange then
				CastSpell(_R, ts.target)
			end
		end
	end
end

function OnDraw()
	if THConfig.drawcirclesSelf and not myHero.dead then
		DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x00FFFF)
		DrawCircle(myHero.x, myHero.y, myHero.z, eRange, 0x00FF00)
	end
	
	if THConfig.drawcirclesEnemy and ts.target ~= nil then
		for j=0, 10 do
			DrawCircle(ts.target.x, ts.target.y, ts.target.z, 40 + j*1.5, 0x00FF00)
		end
		if BlockPos ~= nil and not ts.target.dead then
			DrawCircle(BlockPos.x, BlockPos.y, BlockPos.z, 125, 0x00FFFF)
		end
	end
end

function BlockwithPillar()
	BlockPos = nil
	if PredictedDirection() == "Flee" and THConfig.dynaPillar and ts.target ~= nil then
		TargetPos = Vector(ts.target.x, ts.target.y, ts.target.z)
		MyPos = Vector(myHero.x, myHero.y, myHero.z)
		BlockPos = TargetPos + (TargetPos-MyPos)*((-250/GetDistance(ts.target)))
		elseif THConfig.dynaPillar then
			TargetPos = Vector(ts.target.x, ts.target.y, ts.target.z)
			MyPos = Vector(myHero.x, myHero.y, myHero.z)
			BlockPos = TargetPos + (TargetPos-MyPos)*((250/GetDistance(ts.target)))
	end
	if not THConfig.dynaPillar and ts.target ~= nil then
		TargetPos = Vector(ts.target.x, ts.target.y, ts.target.z)
		MyPos = Vector(myHero.x, myHero.y, myHero.z)
		BlockPos = TargetPos + (TargetPos-MyPos)*((250/GetDistance(ts.target)))
	end
	
	if BlockPos ~= nil and GetDistance(BlockPos) <= eRange and THConfig.pillarBlock and ts.target ~= nil then
		CastSpell(_E, BlockPos.x, BlockPos.z)
	end
end

--[[	Movement Prediction	]]--
function PredictedDirection()
	if ts.target ~= nil then
		local PredictedMovement =  NewPos(PredictedPos())
		if GetDistance(ts.target) > PredictedMovement then return "Flee"        
			elseif GetDistance(ts.target) < PredictedMovement then return "Chase"
		end
	end
end

function NewPos(target)
	if target ~= nil then
		return math.sqrt((myHero.x-target.x)^2+(myHero.z-target.z)^2)
	end
end

function PredictedPos()
	if ts.target ~= nil then
		return GetPredictionPos(ts.target, 50)
	end
end