--[[    Jarvan Helper by HeX/Vadash  ]]--
if myHero.charName ~= "JarvanIV" then return end
--[[    Ranges  ]]--
local wRange = 600
local eDelay = 200 --[[Q cast delay (200 ms), cant lower it]] + 0 --[[E cast delay (200 ms), but we cast Q right after E, before flag lands]] + 0 --[[overshoot 0..50 ms]]
local eBuffer = 50
local Range = 850
local QRange = 850
--[[    Code    ]]--
local Dstandard
local ignite = nil
local QREADY, WREADY, EREADY = false, false, false
local BRKSlot, DFGSlot, HXGSlot, BWCSlot, TMTSlot, RAHSlot, RNDSlot = nil, nil, nil, nil, nil, nil, nil
local BRKREADY, DFGREADY, HXGREADY, BWCREADY, TMTREADY, RAHREADY, RNDREADY = false, false, false, false, false, false, false
--[[    Attacks ]]--
local lastBasicAttack = 0
local swing = 0
local startAttackSpeed = 0.658
local nextTick = 0
if VIP_USER then
	tpE = TargetPredictionVIP(Range, math.huge, eDelay/1000, eBuffer)
	PrintChat("<font color='#CCCCCC'> >> VIP Jarvan Helper loaded! <<</font>")
else
	tpE = TargetPrediction(Range, math.huge, eDelay, eBuffer)
	PrintChat("<font color='#CCCCCC'> >> Basic Jarvan Helper loaded! <<</font>")
end

function OnLoad()
	JHConfig = scriptConfig("Jarvan Helper", "JarvanHelper")
	JHConfig:addParam("scriptActive", "Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	JHConfig:addParam("escape", "Escape Mode", SCRIPT_PARAM_ONKEYDOWN, false, 71)
	JHConfig:addParam("Harass", "Harass", SCRIPT_PARAM_ONKEYDOWN, false, 90)
	JHConfig:addParam("qKillsteal", "KS with Q", SCRIPT_PARAM_ONOFF, true)
	JHConfig:addParam("mouseCombo", "Aim at mouse in Combo", SCRIPT_PARAM_ONOFF, false)
	JHConfig:addParam("autoignite", "Auto Ignite Killable", SCRIPT_PARAM_ONOFF, true)
	JHConfig:addParam("drawcirclesSelf", "Draw Circles - Self", SCRIPT_PARAM_ONOFF, false)
	JHConfig:addParam("drawcirclesEnemy", "Draw Circles - Enemy", SCRIPT_PARAM_ONOFF, false)
	JHConfig:permaShow("scriptActive")
	JHConfig:permaShow("Harass")
	ts = TargetSelector(TARGET_LOW_HP_PRIORITY, Range+50, DAMAGE_PHYSICAL, true)
	ts.name = "JarvanIV"
	JHConfig:addTS(ts)

	if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
		elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2
	end
end
 
function GetFurthest()
	MyPos = Vector(myHero.x, myHero.y, myHero.z)
	MousePos = Vector(mousePos.x, mousePos.y, mousePos.z)
	return MyPos - (MyPos - MousePos):normalized() * 850
end

function OnTick()
	ts:update()
	BRKSlot, DFGSlot, HXGSlot, BWCSlot, TMTSlot, RAHSlot, RNDSlot = GetInventorySlotItem(3153), GetInventorySlotItem(3128), GetInventorySlotItem(3146), GetInventorySlotItem(3144), GetInventorySlotItem(3077), GetInventorySlotItem(3074),  GetInventorySlotItem(3143)
	DFGREADY = (DFGSlot ~= nil and myHero:CanUseSpell(DFGSlot) == READY)
	HXGREADY = (HXGSlot ~= nil and myHero:CanUseSpell(HXGSlot) == READY)
	BWCREADY = (BWCSlot ~= nil and myHero:CanUseSpell(BWCSlot) == READY)
	BRKREADY = (BRKSlot ~= nil and myHero:CanUseSpell(BRKSlot) == READY)
	TMTREADY = (TMTSlot ~= nil and myHero:CanUseSpell(TMTSlot) == READY)
	RAHREADY = (RAHSlot ~= nil and myHero:CanUseSpell(RAHSlot) == READY)
	RNDREADY = (RNDSlot ~= nil and myHero:CanUseSpell(RNDSlot) == READY)
	IREADY = (ignite ~= nil and myHero:CanUseSpell(ignite) == READY)
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
 
	--[[    Auto Ignite     ]]--
	if JHConfig.autoignite then    
		if IREADY then
			local ignitedmg = 0    
			for i = 1, heroManager.iCount, 1 do
				local enemyhero = heroManager:getHero(i)
				if ValidTarget(enemyhero,600) then
					ignitedmg = 50 + 20 * myHero.level
					if enemyhero.health <= ignitedmg then
						CastSpell(ignite, enemyhero)
					end
				end
			end
		end
	end
	
	--[[ Q Killsteal	]]--
	if JHConfig.qKillsteal then    
		if QREADY then
			local qDmg = 0    
			for i = 1, heroManager.iCount, 1 do
				local enemyhero = heroManager:getHero(i)
				if ValidTarget(enemyhero, 770) then
					qDmg = getDmg("Q", enemyhero, myHero)
					if enemyhero.health <= qDmg then
						predic = tpE:GetPrediction(enemyhero)
						hc = tpE:GetHitChance(enemyhero)
						if predic and hc > 0.5 then CastSpell(_Q, predic.x, predic.z) end
					end
				end
			end
		end
	end
	
	--[[    Prediction      ]]--
	if ts.target ~= nil and ValidTarget(ts.target) then
		predic = tpE:GetPrediction(ts.target)
		hc = tpE:GetHitChance(ts.target)
	end
	if predic ~= nil and hc > 0.5 and not myHero.dead and ts.target ~= nil and ValidTarget(ts.target) then
		TargetPos = Vector(predic.x, predic.y, predic.z)
		MyPos = Vector(myHero.x, myHero.y, myHero.z)
		TruePos = TargetPos + (TargetPos-MyPos)*((eBuffer/GetDistance(ts.target)))
	end

	--[[    Harass  ]]--
	if JHConfig.Harass then
		if ts.target ~= nil and GetDistance(ts.target) < 770 then
			predic = tpE:GetPrediction(ts.target)
			hc = tpE:GetHitChance(ts.target)
			if predic ~= nil and hc > 0.5 then CastSpell(_Q, predic.x, predic.z) end
		end
	end

	--[[    Items   ]]--
	if JHConfig.scriptActive and ts.target and ValidTarget(ts.target) and GetDistance(ts.target) < wRange then
		if DFGREADY then CastSpell(DFGSlot, ts.target) end
		if HXGREADY then CastSpell(HXGSlot, ts.target) end
		if BWCREADY then CastSpell(BWCSlot, ts.target) end
		if BRKREADY then CastSpell(BRKSlot, ts.target) end
		if TMTREADY and GetDistance(ts.target) < 275 then CastSpell(TMTSlot) end
		if RAHREADY and GetDistance(ts.target) < 275 then CastSpell(RAHSlot) end
		if RNDREADY and GetDistance(ts.target) < 275 then CastSpell(RNDSlot) end
	end
 
	--[[	Escape / mousepos combo	]]--
	if (JHConfig.scriptActive and JHConfig.mouseCombo) or JHConfig.escape then
		if EREADY and QREADY then
			if GetDistance(mousePos) <= Range then
				CastSpell(_E, mousePos.x, mousePos.z)
				Dstandard = mousePos
			else
				CastSpell(_E, GetFurthest().x, GetFurthest().z)
				Dstandard = GetFurthest()
			end
		end
		if Dstandard ~= nil then
			if QREADY and GetDistance(Dstandard) < Range then
				local QPos = myHero - (Vector(myHero) - Vector(Dstandard)):normalized()*QRange
				CastSpell(_Q, QPos.x, QPos.z)
				Dstandard = nil
			end
		end            
	end

	--[[ Auto combo ]]--
	if JHConfig.scriptActive and ts.target and ValidTarget(ts.target) and not JHConfig.mouseCombo then
		if TruePos ~= nil and EREADY and QREADY and GetDistance(TruePos) < Range and not JHConfig.mouseCombo then
			CastSpell(_E,TruePos.x,TruePos.z)
			Dstandard = TruePos
		end
		if Dstandard ~= nil then
			if QREADY and GetDistance(Dstandard) < Range and GetDistance(TruePos, Dstandard) < 75 then
				local QPos = myHero - (Vector(myHero) - Vector(Dstandard)):normalized()*QRange
				CastSpell(_Q, QPos.x, QPos.z)
				Dstandard = nil
			end
		end
	end
end
 
function OnCreateObj(obj)
	if obj ~= nil and (string.find(obj.name, "JarvanDemacianStandard_tar") or string.find(obj.name, "JarvanDemacianStandard_buf")) then
		Dstandard = obj
	end
end
 
function OnDeleteObj(obj)
	if obj ~= nil and (string.find(obj.name, "JarvanDemacianStandard_tar") or string.find(obj.name, "JarvanDemacianStandard_buf")) then
		Dstandard = nil
	end
end
 
function OnDraw()      
	if JHConfig.drawcirclesSelf and not myHero.dead then
		DrawCircle(myHero.x, myHero.y, myHero.z, Range, 0x00FF00)
	end
	if ts.target ~= nil and JHConfig.drawcirclesEnemy then
		for j=0, 10 do
			DrawCircle(ts.target.x, ts.target.y, ts.target.z, 40 + j*1.5, 0x00FF00)
		end
	end
end